require 'drb'
require 'highline/import'
require 'choice'

Choice.options do

  option :host do
    long '--host HOST'
    short '-h'
    default 'localhost'
  end

  option :port do
    long '--port PORT'
    short '-p'
    cast Integer
    default 9000
  end

  option :nick do
    long '--nick NICK'
    short '-n'
  end

  option :password do
    long '--password PASSWORD'
    short '-P'
  end

end

class ChatWriter

  def initialize(host, port, nick, password)
    @server = DRbObject.new_with_uri("druby://#{host}:#{port}")
    @nick = nick
    @password = password
  end

  def start
    @thread = Thread.new do
      send("#{@nick} joined the room")
      loop do
        print '> '
        message = "#{@nick}: #{STDIN.gets.chomp}"
        send(message)
      end
    end
  end

  def send(message)
    @server.push(@password, "#{Time.now.strftime('%H:%M:%S GMT%:::z')} | #{message}")
  end

  def quit
    @thread.kill
    send("#{@nick} left the room")
  end
end

nick = Choice[:nick] || ask('Nick: ')
password = Choice[:password] || ask('Password: ') { |q| q.echo = '*' }
writer = ChatWriter.new(Choice[:host], Choice[:port], nick, password)
begin
  writer.start.join
rescue Interrupt
  writer.quit
end
