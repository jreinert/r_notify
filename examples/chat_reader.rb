require 'drb'
require 'highline/import'
require 'choice'

Choice.options do

  option :host do
    long '--host HOST'
    short '-h'
    default 'localhost'
  end

  option :port do
    long '--port PORT'
    short '-p'
    cast Integer
    default 9000
  end

  option :password do
    long '--passwort PASSWORD'
    short '-P'
  end

end

class ChatReader

  def initialize(host, port, password)
    @server = DRbObject.new_with_uri("druby://#{host}:#{port}")
    @password = password
  end

  def start
    loop do
      message = @server.listen(@password)
      puts message unless message == :heartbeat
    end
  end

end

password = Choice[:password] || ask('Password: ') {|q| q.echo = '*'}
reader = ChatReader.new(Choice[:host], Choice[:port], password)
trap('INT') { exit }
reader.start
