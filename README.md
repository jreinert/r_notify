# RNotify

RNotify is a notification library written in ruby. It consists of a server and client code which communicate with each other over drb.

## Installation

Add this line to your application's Gemfile:

    gem 'r_notify'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install r_notify

## Usage

Start a server by running `rnotify-server`. You will be prompted for a password which clients will use to authenticate.

Start a client by running `rnotify-client`. You will again be prompted for a password which will be used to authenticate on the server. Any notifications sent to the server using its push method will be shown on the client.

Example chat program:

1. Start a server
2. Run following code on clients:

``` ruby
require 'drb'

class ChatReader

  def initialize(host, port, password)
    @server = DRbObject.new_with_uri("druby://#{host}:#{port}")
    @password = password
  end

  def start
    loop do
      message = @server.listen(@password)
      puts message unless message == :heartbeat
    end
  end

end

class ChatWriter

  def initialize(host, port, nick, password)
    @server = DRbObject.new_with_uri("druby://#{host}:#{port}")
    @nick = nick
    @password = password
  end

  def start
    loop do
      print '> '
      message = "#{@nick}: #{STDIN.gets.chomp}"
      @server.push(@password, message)
    end
  end
end
```

You get the picture...

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
