module RNotify
  class Server

    def initialize(password)
      @notification_queue = Queue.new
      @password = password
      Thread.new do
        loop do
          push(:heartbeat)
          sleep 30
        end
      end
    end

    def listen(password)
      authenticate password
      @notification_queue.pop
    end

    def push(password, notification)
      authenticate password
      @notification_queue.push(notification)
    end

    class AuthError < SecurityError; end

    private

    def authenticate(password)
      sleep 1 # slow down brute forcing
      raise AuthError unless password == @password
    end

  end
end
