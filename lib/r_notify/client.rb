module RNotify
  class Client
    require 'gir_ffi'
    require "drb"

    def initialize(server_uri, password)
      @server_uri = server_uri
      @server = DRbObject.new_with_uri(@server_uri)
      @password = password
      GirFFI.setup :Notify
      Notify.init("Rubote notify")
    end

    def start
      puts "Listening for notifications from #{@server_uri}"
      loop do
        message = @server.listen(@password)
        next if message == :heartbeat
        notification(message).show
      end
    end

    private

    def notification message
      buffer = message[:buffer]
      prefix = message[:prefix]
      message = message[:message]
      Notify::Notification.new(
        "<i>#{buffer || "Query"}:</i>",
        "<b>#{prefix}</b> | #{message}",
        "dialog-information"
      )
    end

  end
end
