# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'r_notify/version'

Gem::Specification.new do |spec|
  spec.name          = "r_notify"
  spec.version       = RNotify::VERSION
  spec.authors       = ["Joakim Reinert"]
  spec.email         = ["mail@jreinert.com"]
  spec.description   = %q{r_notify is a notification library using drb.}
  spec.summary       = %q{r_notify consists of a server and a client. Multiple clients can connect to a server and exchange notifications.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_dependency "highline"
  spec.add_dependency "choice"
  spec.add_dependency "gir_ffi"

end
